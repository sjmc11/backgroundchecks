<?
/***
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
****/

#change post author in array when publishing, please.

include('IXR_Library.php');
$usr = 'Barry';
$pwd = 'jHx*fUezdzUL1fdbTapVBsxi';
$xmlrpc = 'http://www.cdn06.com/xmlrpc.php';
$client = new IXR_Client($xmlrpc);
$client -> debug = true; //optional but useful
$servername = "45.55.38.170";
$username = "root";
$password = "Wh1LF1Nd0i";
$dbname = "finance_publisher";
$api_key ='.json?api_key=H-gDVVC8MxMhzs9DHVNu';



function analyze($zack_url,$ticker,$api_key){

$url = $zack_url .$ticker .$api_key;
$data = json_decode(file_get_contents($url),true);
return $data;

}


// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

#this is for top 1800 stocks
$sql = "SELECT * from top_tickers  ";
#$sql = "SELECT * from stocks where exchange =" ."'NYSE'"  ."limit 1000";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {


$query = "SELECT dataset_code,description from blurbs WHERE ticker = '$row[ticker]' ";
$query_result = $conn->query($query);
while($data = $query_result->fetch_assoc())

{
$short_query = "SELECT * from shortdata WHERE SYMBOL = '$row[ticker]' ";
$short_query_result = $conn->query($short_query);
while($shortdata = $short_query_result->fetch_assoc()){
$blurb =  $data['description'];
$ticker = $row['ticker'];
$yahoourl = 'http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20IN%20%28%22' .$ticker .'%22%29&format=json&env=http://datatables.org/alltables.env';
$imgurl = 'http://chart.finance.yahoo.com/z?s=' .$ticker .'&t=6m&q=l&z=l';
$yahoo = json_decode(file_get_contents($yahoourl),true);
$tickermarket = str_replace('_',':',$data['dataset_code']); 
$company = $shortdata['COMPANY'];

$zack_eps = analyze('https://www.quandl.com/api/v3/datasets/ZEE/', $ticker .'_Q', $api_key);
$zack_zss = analyze('https://www.quandl.com/api/v3/datasets/ZSS/', $ticker, $api_key);
$zack_zea  = analyze('https://www.quandl.com/api/v3/datasets/ZEA/', $ticker, $api_key);
$zack_zar  = analyze('https://www.quandl.com/api/v3/datasets/ZAR/', $ticker, $api_key);
$zack_zes = analyze('https://www.quandl.com/api/v3/datasets/ZES/', $ticker, $api_key);


#var_dump($zack_eps);

$quarter1 = date("F j, Y",strtotime($zack_zea['dataset']['data'][0][4]));
$quarter2 = date("F j, Y",strtotime($zack_zea['dataset']['data'][0][5]));
$fy = date("F j, Y",strtotime($zack_zea['dataset']['data'][0][6]));
$title =  "New Analyst Updates for $company";
$keywords = <<< EX
{$tickermarket}, {$company}, {$company} analysis and updates, $tickermarket ratings, $company ratings, $company earnings expectations, $company analyst ratings, $ticker earnings, $ticker earnings expectations
EX;

#echo $keywords;
$custom_fields = array(
                   array('key' => '_yoast_wpseo_newssitemap-stocktickers', 'value' => "$tickermarket"),
                   array('key' => '_yoast_wpseo_newssitemap-keywords', 'value' => $keywords)
                 );
$params = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'post_title' => $title,
    'post_author' => 8,
    'post_excerpt' => '',
   'terms_names'  => array( 
                'category' => array( 'business' )),
'custom_fields' => $custom_fields);



$params['post_content'] = <<<EX




<h2>Analysts Expect  {$zack_eps['dataset']['data'][0][1]} Earnings Per Share </h2>



In a recent survey,  {$zack_eps['dataset']['data'][0][5]} analysts were expecting an average of \${$zack_eps['dataset']['data'][0][1]} earnings per share for {$company}, for the {$zack_eps['dataset']['data'][0][9]} quarter of the fiscal year ending
in {$zack_eps['dataset']['data'][0][8]}.<p>
<br />
{$ticker} and {$company} stock and share performance over the last 6 months.
<p>
 Among the analysts, the highest EPS was \${$zack_eps['dataset']['data'][0][3]}  and the lowest was \${$zack_eps['dataset']['data'][0][4]}. This represents a {$zack_eps['dataset']['data'][0][7]}% change for the EPS reported for the same quarter in the prior year. This equates to the consensus earnings growth estimate for the last 12 months, and should not be mistaken for a long term growth estimate.
Also, because of different datasources, our reporting and figures may be different then the numbers reported by FactSet and other sources.



<h2>Can {$company} ({$tickermarket}) hit expected sales?</h2> 

In its most recent quarter $company had actual sales of $ {$zack_zss[dataset][data][0][8]}M. Among the {$zack_zss[dataset][data][0][13]} analysts who were surveyed, the consensus expectation for quarterly sales had been
{$zack_zss[dataset][data][0][10]}M. This represents a {$zack_zss[dataset][data][0][12]}% difference between analyst expectations and the $company achieved in its quarterly earnings.

We've also learned that $company will report its next earnings on {$quarter1}. The earnings report after that one will be on {$quarter2}, and the report for the fiscal year will be made available on {$fy}.
<br>
Last quarters actual earnings were {$zack_zea['dataset']['data'][0][10]} per share.
<br />




<h2> Analysts continue to rate {$company} </h2>

Sell side brokers and analysts continue to rate {$company}:

The overall rating for the company is {$zack_zar['dataset']['data'][0][21]}. The rating is an average of the various different ratings given by analysts and brokers to {$company}, and then averaged into one rating by a team of analysts at Zacks in Chicago, Illinois.
For $company, the numerical average rating system is as follows:
<ol>
<li><strong>Strong buy</strong> for $ticker</li>
<li><strong>BUY</strong> for $ticker</li>
<li><strong>HOLD</strong> for $ticker</li>

<li><strong>SELL</strong> for $ticker</li>
<li><strong>Strong SELL</strong> for $ticker</li>
</ol>



<h2>{$company} earnings estimates and actual figures</h2>


{$company}'s graph of trading over the last several months:
<img src ="{$imgurl}">


{$company} most recently announcied its earnings on {$zack_zes['dataset']['data'][0][0]}. After surveying {$zack_zes['dataset']['data'][0][7]} different analysts, we established an average estimate of
$ {$zack_zes['dataset']['data'][0][1]} earnings per share (EPS) for {$tickermarket}. Actual earnings share as last reported was {$zack_zes['dataset']['data'][0][1]}.



<ul>
<li>{$zack_zar['dataset']['data'][0][2]} Analysts rate the company a <strong>strong buy</strong></li>
<li>{$zack_zar['dataset']['data'][0][5]}  Analysts rate the company a <strong>buy</strong> </li>
<li>{$zack_zar['dataset']['data'][0][9]}  Analysts rate the company a <strong>hold</strong></li>

<li>{$zack_zar['dataset']['data'][0][13]}  Analysts rate the company a <strong>sell</strong></li>
<li>{$zack_zar['dataset']['data'][0][17]}  Analysts rate the company a <strong>strong sell</strong></li>
</ul>







</p>


In its most recent quarter $company had actual sales of $ {$zack_zss[dataset][data][0][8]}M. Among the {$zack_zss[dataset][data][0][13]} analysts who were surveyed, the consensus expectation for quarterly sales had been
{$zack_zss[dataset][data][0][10]}M. This represents a {$zack_zss[dataset][data][0][12]}% difference between analyst expectations and the $company achieved in its quarterly earnings.

We've also learned that $company will report its next earnings on {$quarter1}. The earnings report after that one will be on {$quarter2}, and the report for the fiscal year will be made available on {$fy}.
<br>
Last quarters actual earnings were {$zack_zea['dataset']['data'][0][10]} per share.
<br />


<h2>{$company} Historical Information </h2>
Historically, {$company} has been trading with a 52 week low of {$yahoo['query']['results']['quote']['YearLow']} and a 52 week of high {$yahoo['query']['results']['quote']['YearHigh']}. Technical indicators show a 50 day 
moving average of {$yahoo['query']['results']['quote']['FiftydayMovingAverage']}. Recent trading put {$company} stock at a {$yahoo['query']['results']['quote']['ChangeFromFiftydayMovingAverage']} change from the 50 day moving average, which is 
{$yahoo['query']['results']['quote']['PercentChangeFromFiftydayMovingAverage']}.


<br>
Additionally, {$company} currently has a market capitalization of {$yahoo['query']['results']['quote']['MarketCapitalization']}. {$company} Reported earnings before interest, taxes, debt and amortization (EBITDA) is {$yahoo['query']['results']['quote']['EBITDA']}. Earnings per share were {$yahoo['query']['results']['quote']['EarningsShare']}.
</p>


In its most recent quarter $company had actual sales of $ {$zack_zss[dataset][data][0][8]}. Among the {$zack_zss[dataset][data][0][13]} analysts who were surveyed, the consensus expectation for quarterly sales had been
{$zack_zss[dataset][data][0][10]}. This represents a {$zack_zss[dataset][data][0][12]}% difference between analyst expectations and the $company achieved in its quarterly earnings.







<p>
{$blurb}
</p>
<p>




EX;

$res = $client-> query('wp.newPost',1, $usr, $pwd, $params);

echo $client->getResponse();

#echo $params['post_content'];
#echo $title;

}


}


    }
} else {
    echo "0 results";
}
$conn->close();
?> 