<?php
/**
This file is part of the Textbroker WordPress-Plugin.

The Textbroker WordPress-Plugin is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

The Textbroker WordPress Plugin is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with the Textbroker WordPress Plugin.  If not, see http://www.gnu.org/licenses/.
*/

/**
 *
 * @package Textbroker WordPress-Plugin
 * @author  Fabio Bacigalupo <info1@open-haus.de>
 * @copyright Fabio Bacigalupo 2011
 * @version $Revision: 2.0 $
 * @since PHP5.2.12
 */
class TextbrokerOrder extends TextbrokerPlugin {

    /**
     * Singleton
     *
     * @return obj
     */
    public static function &singleton() {

        static $instance;

        if (!isset($instance)) {
            $class      = __CLASS__;
            $instance   = new $class;
        }
        return $instance;
    }

    /**
     *
     *
     */
    public function process() {

        $showOrderInformation = true;

        try {
            switch ( $_REQUEST['_wpnonce'] ) {
                case wp_create_nonce(parent::ACTION_ORDER_ADD) :
                    $this->showOrderForm($_REQUEST[parent::PARAM_BUDGET_ID], $_REQUEST[parent::PARAM_BUDGET_NAME], $_REQUEST[parent::PARAM_ORDER], false, true);
                    $showOrderInformation = false;
                    break;
                case wp_create_nonce(parent::ACTION_ORDER_INSERT) :
                    $this->saveOrder($_REQUEST[parent::PARAM_ORDER]);
                    $this->showMessage(__('Order saved successfully', parent::getIdentifier()), self::HINT_SUCCESS, true);
                    break;
                case wp_create_nonce(parent::ACTION_ORDER_GETSTATUS) :
                    $this->getStatus($_REQUEST[parent::PARAM_BUDGET_ID], $_REQUEST[parent::PARAM_ORDER_ID]);
                    $this->showMessage(__('Order status updated successfully', parent::getIdentifier()), self::HINT_SUCCESS, true);
                    break;
                case wp_create_nonce(parent::ACTION_ORDER_ACCEPT) :
                    $this->acceptOrder($_REQUEST[parent::PARAM_ORDER_ID], $_REQUEST[parent::PARAM_ORDER_RATING], $_REQUEST[parent::PARAM_ORDER_COMMENT]);
                    $this->showMessage(__('Order accepted successfully', parent::getIdentifier()), self::HINT_SUCCESS, true);
                    break;
                case wp_create_nonce(parent::ACTION_ORDER_PREVIEW) :
                    $this->showPreview($this->previewOrder($_REQUEST[parent::PARAM_ORDER_ID]), true);
                    $showOrderInformation = false;
                    break;
                case wp_create_nonce(parent::ACTION_ORDER_REVISE) :
                    $this->reviseOrder($_REQUEST[parent::PARAM_ORDER_ID], $_REQUEST[parent::PARAM_COMMENT]);
                    $this->showMessage(__('Order revision requested successfully', parent::getIdentifier()), self::HINT_SUCCESS, true);
                    break;
                case wp_create_nonce(parent::ACTION_ORDER_PUBLISH) :
                    $this->publishOrder($_REQUEST[parent::PARAM_ORDER_ID], $_REQUEST[parent::PARAM_PUBLISH_TYPE]);
                    $this->showMessage(__('Order published successfully', parent::getIdentifier()), self::HINT_SUCCESS, true);
                    break;
                case wp_create_nonce(parent::ACTION_ORDER_DELETE) :
                    $this->deleteOrder($_REQUEST[parent::PARAM_ORDER_ID]);
                    $this->showMessage(__('Order deleted successfully', parent::getIdentifier()), self::HINT_SUCCESS, true);
                    break;
                case wp_create_nonce(parent::ACTION_ORDER_REMOVE) :
                    $this->removeOrder($_REQUEST[parent::PARAM_ORDER_ID]);
                    $this->showMessage(__('Order removed successfully', parent::getIdentifier()), self::HINT_SUCCESS, true);
                    break;
                case wp_create_nonce(parent::ACTION_ORDER_REJECT) :
                    $this->rejectOrder($_REQUEST[parent::PARAM_ORDER_ID]);
                    $this->deleteOrder($_REQUEST[parent::PARAM_ORDER_ID]);
                    $this->showMessage(__('Order rejected successfully', parent::getIdentifier()), self::HINT_SUCCESS, true);
                    break;
                case wp_create_nonce(parent::ACTION_ORDER_EDIT) :
                    $aOrder = $this->getOrder($_REQUEST[parent::PARAM_ORDER_ID]);
                    $aBudget = $this->getBudget($aOrder['budget_id']);
                    $this->showOrderForm($aOrder['budget_id'], $aBudget['name'], $aOrder, true, true);
                    $showOrderInformation = false;
                    break;
                case wp_create_nonce(parent::ACTION_ORDER_UPDATE) :
                    $this->saveOrder($_REQUEST[parent::PARAM_ORDER], true);
                    $this->showMessage(__('Order saved successfully', parent::getIdentifier()), self::HINT_SUCCESS, true);
                    break;
                default :
                    $this->updateOrders();
            }
        } catch (TextbrokerOrderInsertException $e) {
            $this->showMessage($e->getMessage(), self::HINT_ERROR, true);
            $this->showOrderForm($_REQUEST[parent::PARAM_BUDGET_ID], $_REQUEST[parent::PARAM_BUDGET_NAME], $_REQUEST[parent::PARAM_ORDER], false, true);
            $showOrderInformation = false;
        } catch (TextbrokerOrderUpdateException $e) {
            $this->showMessage($e->getMessage(), self::HINT_ERROR, true);
            $this->showOrderForm($_REQUEST[parent::PARAM_BUDGET_ID], $_REQUEST[parent::PARAM_BUDGET_NAME], $_REQUEST[parent::PARAM_ORDER], true, true);
            $showOrderInformation = false;
        } catch (TextbrokerOrderReviseException $e) {
            $this->showMessage($e->getMessage(), self::HINT_ERROR, true);
            $this->showPreview($this->previewOrder($_REQUEST[parent::PARAM_ORDER_ID]), true);
            $showOrderInformation = false;
        } catch (Exception $e) {
            $this->showMessage($e->getMessage(), self::HINT_ERROR, true);
        }

        if ($showOrderInformation) {
            $this->showOrderInformation($this->getOrders(), true);
        }
    }

/******************************************************************************************************************************************************************************
 * Private processing stuff
 ******************************************************************************************************************************************************************************/

    /**
     *
     *
     * @param int $budgetId
     * @return array
     */
    private function getCategories($budgetId) {

        $aBudget = $this->getBudget($budgetId);

        return TextbrokerBudgetOrder::singleton($aBudget['key'], $aBudget['id'], $aBudget['password'], $aBudget['location'])->getCategories();
    }

    /**
     *
     * @param array $aOrder
     * @param bool $update
     * @throws TextbrokerBudgetInsertException
     */
    private function saveOrder(array $aOrder, $update = false) {

        $aError = array();

	    if (!$aOrder['order_title']) {
	    	$aError[] = __('ERROR: You have to provide an order title', parent::getIdentifier());
	    }

	    if (!$aOrder['category_id'] || !is_numeric($aOrder['category_id'])) {
	    	$aError[] = __('ERROR: You have to select a category', parent::getIdentifier());
	    }

	    if (!$aOrder['order_text']) {
	    	$aError[] = __('ERROR: You have to provide an order text', parent::getIdentifier());
	    }

	    if (!$aOrder['words_min'] || !is_numeric($aOrder['words_min']) || $aOrder['words_min'] < 1) {
	    	$aError[] = __('ERROR: Words minimum is 100 words', parent::getIdentifier());
	    }

	    if ($aOrder['words_min'] > $aOrder['words_max']) {
	    	$aError[] = __('ERROR: Maximum words cannot be smaller than minimum words', parent::getIdentifier());
	    }

	    if (!$aOrder['duedays'] || !is_numeric($aOrder['duedays']) || $aOrder['duedays'] < 1 || $aOrder['duedays'] > 10) {
	    	$aError[] = __('ERROR: Due days have to be set to 1 or more days', parent::getIdentifier());
	    }

	    if (!$aOrder['rating'] || !is_numeric($aOrder['rating']) || $aOrder['rating'] < 2) {
	    	$aError[] = __('ERROR: Rating has to be chosen', parent::getIdentifier());
	    }

	    if (count($aError) > 0) {

            array_walk($aError, create_function('&$val', '$val = "<li>$val</li>";'));
            $error = '<ul>' . implode('', $aError) . '</ul>';

	    	if ($update) {
	    		throw new TextbrokerOrderUpdateException($error);
	    	} else {
                throw new TextbrokerOrderInsertException($error);
	    	}
	    }

    	try {
    	    if ($update) {
    	    	$this->deleteOrder($aOrder['order_id']);
    	    }

    	    $aBudget                    = $this->getBudget($aOrder['budget_id']);
            $oBudgetOrder               = TextbrokerBudgetOrder::singleton($aBudget['key'], $aBudget['id'], $aBudget['password'], $aBudget['location']);
            $aOrder['budget_order_id']  = $oBudgetOrder->create($aOrder['category_id'], $aOrder['order_title'], $aOrder['order_text'], $aOrder['words_min'], $aOrder['words_max'], $aOrder['rating'], (int)$aOrder['duedays']);
            $aOrder['status']           = $oBudgetOrder->getStatus($aOrder['budget_order_id']);
            $aOrder['order_date']       = $this->getDate($aBudget['location']);
            $this->setOrder($aOrder);
    	} catch (TextbrokerBudgetOrderException $e) {
	    	if ($update) {
	    		throw new TextbrokerOrderUpdateException($error);
	    	} else {
                throw new TextbrokerOrderInsertException($error);
	    	}
    	}
    }

    /**
     *
     *
     * @param int $orderId
     * @return array
     * @throws TextbrokerOrderException
     */
    protected function getOrder($orderId) {

        $aOrders = $this->getOption(parent::IDENTIFIER_ORDERS);

        if(!isset($aOrders[$orderId])) {
            throw new TextbrokerOrderException('Failed getting order');
        }

        return $aOrders[$orderId];
    }

    /**
     *
     *
     * @param array $aOrder
     */
    private function setOrder(array $aOrder) {

        $aOrders                                = $this->getOrders();
        $aOrders[$aOrder['budget_order_id']]    = $aOrder;
        $this->setOrders($aOrders);
    }

    /**
     *
     *
     * @return array
     */
    private function getOrders() {

        $aOrders = $this->getOption(parent::IDENTIFIER_ORDERS);

        if(!is_array($aOrders)) {
            return array();
        }

        return $aOrders;
    }

    /**
     *
     *
     * @param array $aOrders
     */
    private function setOrders(array $aOrders) {

        $this->setOption(self::IDENTIFIER_ORDERS, $aOrders);
    }

    /**
     *
     *
     * @param int $budgetId
     * @param int $orderId
     * @return array
     */
    private function getStatus($budgetId, $orderId) {

	    $aBudget                    = $this->getBudget($budgetId);
        $oBudgetOrder               = TextbrokerBudgetOrder::singleton($aBudget['key'], $aBudget['id'], $aBudget['password'], $aBudget['location']);
        $aStatus                    = $oBudgetOrder->getStatus($orderId);
        $aOrder                     = $this->getOrder($orderId);
        $aOrder['status']           = $aStatus;
        $this->setOrder($aOrder);

        return $aStatus;
    }

    /**
     *
     *
     * @param int $orderId
     * @param int $rating
     * @param string $comment
     * @return bool
     */
    private function acceptOrder($orderId, $rating, $comment = null) {

        $aOrder                     = $this->getOrder($orderId);
	    $aBudget                    = $this->getBudget($aOrder['budget_id']);
        $oBudgetOrder               = TextbrokerBudgetOrder::singleton($aBudget['key'], $aBudget['id'], $aBudget['password'], $aBudget['location']);
        $aResult                    = $oBudgetOrder->accept($orderId, $rating, $comment);
        $aOrder['title']            = $aResult['title'];
        $aOrder['text']             = $aResult['text'];
        $aOrder['author']           = $aResult['author'];
        $aStatus                    = $oBudgetOrder->getStatus($orderId);
        $aOrder['status']           = $aStatus;

        return $this->setOrder($aOrder);
    }

    /**
     *
     *
     * @param int $orderId
     * @return array
     */
    private function previewOrder($orderId) {

        $aOrder                     = $this->getOrder($orderId);
	    $aBudget                    = $this->getBudget($aOrder['budget_id']);
        $oBudgetOrder               = TextbrokerBudgetOrder::singleton($aBudget['key'], $aBudget['id'], $aBudget['password'], $aBudget['location']);
        $aOrderPreview              = $oBudgetOrder->preview($orderId);
        $aOrderPreview['order_id']  = $orderId;
        $aOrderPreview['is_revised']= $aOrder['is_revised'];

        return $aOrderPreview;
    }

    /**
     *
     *
     * @param int $orderId
     * @param string $comment
     * @return bool
     */
    private function reviseOrder($orderId, $comment) {

        if (strlen($comment) < 50) {
        	throw new TextbrokerOrderReviseException(__('ERROR: Your feedback has to be at least 50 characters', parent::getIdentifier()));
        }

        $aOrder                     = $this->getOrder($orderId);
	    $aBudget                    = $this->getBudget($aOrder['budget_id']);
        $oBudgetOrder               = TextbrokerBudgetOrder::singleton($aBudget['key'], $aBudget['id'], $aBudget['password'], $aBudget['location']);
        $oBudgetOrder->revise($orderId, $comment);
        $aStatus                    = $oBudgetOrder->getStatus($orderId);
        $aOrder['status']           = $aStatus;
        $aOrder['is_revised']       = true;

        return $this->setOrder($aOrder);
    }

    /**
     *
     *
     * @param int $orderId
     */
    private function rejectOrder($orderId) {

        $aOrder                     = $this->getOrder($orderId);
	    $aBudget                    = $this->getBudget($aOrder['budget_id']);
        $oBudgetOrder               = TextbrokerBudgetOrder::singleton($aBudget['key'], $aBudget['id'], $aBudget['password'], $aBudget['location']);
        $oBudgetOrder->reject($orderId);
    }

    /**
     *
     *
     * @param int $orderId
     * @return bool
     */
    private function publishOrder($orderId, $type) {

        $aOrder                     = $this->getOrder($orderId);
	    $aBudget                    = $this->getBudget($aOrder['budget_id']);
        $oBudgetOrder               = TextbrokerBudgetOrder::singleton($aBudget['key'], $aBudget['id'], $aBudget['password'], $aBudget['location']);
        $aResult                    = $oBudgetOrder->pickUp($orderId);
        $aOrder['title']            = $aResult['title'];
        $aOrder['text']             = $aResult['text'];
        $aOrder['count_words']      = $aResult['count_words'];
        $aOrder['already_delivered']= $aResult['already_delivered'];
        $aStatus                    = $oBudgetOrder->getStatus($orderId);
        $aOrder['status']           = $aStatus;

        if (!defined('parent::PUBLISH_TYPE_' . strtoupper($type))) {
        	throw new TextbrokerOrderPublishException('Invalid publish type');
        }

        // Create post object
        $my_post = array(
            'post_title'       => $aOrder['title'],
            'post_content'     => $aOrder['text'],
            'post_status'      => 'draft',
            'post_type'        => $type,
        );

        // Insert the post into the database
        $id = wp_insert_post( $my_post );

        if (!$id || $id instanceof WP_Error) {
        	throw new TextbrokerOrderPublishException('Failed to publish post');
        }

        $aOrder['published_id'] = $id;

        return $this->setOrder($aOrder);
    }

    /**
     *
     * @param int $orderId
     * @throws TextbrokerOrderException
     */
    private function deleteOrder($orderId) {

        try {
            $aOrder                     = $this->getOrder($orderId);
    	    $aBudget                    = $this->getBudget($aOrder['budget_id']);
            $oBudgetOrder               = TextbrokerBudgetOrder::singleton($aBudget['key'], $aBudget['id'], $aBudget['password'], $aBudget['location']);
            $oBudgetOrder->delete($orderId);
            $aOrders                    = $this->getOrders();
            unset($aOrders[$orderId]);
            $this->setOrders($aOrders);
    	} catch (Exception $e) {
    	    throw new TextbrokerOrderException($e->getMessage());
    	}
    }

    /**
     *
     * @param int $orderId
     * @throws TextbrokerOrderException
     */
    private function removeOrder($orderId) {

        try {
            $aOrders                    = $this->getOrders();
            unset($aOrders[$orderId]);
            $this->setOrders($aOrders);
    	} catch (Exception $e) {
    	    throw new TextbrokerOrderException($e->getMessage());
    	}
    }

    private function updateOrders() {

        $aOrders                        = $this->getOrders();

        foreach ($aOrders as $orderId => $aOrder) {
            $aOrder                     = $this->getOrder($orderId);
    	    $aBudget                    = $this->getBudget($aOrder['budget_id']);
            $oBudgetOrder               = TextbrokerBudgetOrder::singleton($aBudget['key'], $aBudget['id'], $aBudget['password'], $aBudget['location']);
            $aStatus                    = $oBudgetOrder->getStatus($orderId);
            $aOrders[$orderId]['status']= $aStatus;
        }

        $this->setOrders($aOrders);
    }
/******************************************************************************************************************************************************************************
 * HTML forms
 ******************************************************************************************************************************************************************************/

    /**
     *
     * @param bool $display
     * @return void | string
     */
    public function showOrderAddButton($display = false) {

        $str    = '
            <p class="submit">
                <a href="%s" class="button">%s</a>
            </p>
        ';

        $args   = array(
            attribute_escape(add_query_arg('_wpnonce', wp_create_nonce(self::ACTION_ORDER_ADD))),
            __('Add order', parent::getIdentifier()),
        );

        if ($display) {
            vprintf($str, $args);
        } else {
            return vsprintf($str, $args);
        }
    }

    /**
     *
     * @param int $budgetId
     * @param bool $display
     * @return void | string
     */
    public function showOrderForm($budgetId, $budgetName, $aOrder = array(), $update = false, $display = false) {

        $str    = '
            <h4>%s</h4>
            <form method="post" action="%s" class="tb" id="order-form">
                <fieldset>
                    <input type="hidden" id="budget-id" name="budget_id" value="%s" />
                    <input type="hidden" name="budget_name" value="%s" />
                    <input type="hidden" name="order[budget_id]" value="%s" />
                    <input type="hidden" name="order[order_id]" value="%s" />
                </fieldset>
                <fieldset class="left half">
                    <label for="order-category" class="leftLabel">%s
                    <select name="order[category_id]" id="order-category" class="medium">
                        <option value="" />
                        %s
                    </select></label><br />
                    <label for="rating" class="leftLabel">%s
                    <select name="order[rating]" id="rating" class="medium">
                        <option value="" />
                        %s
                    </select></label><br />
                    <label for="duedays" class="leftLabel">%s
                    <select name="order[duedays]" id="duedays" class="medium">
                        <option value="" />
                        %s
                    </select></label><br />
                    <label for="words-min" class="leftLabel">%s <input type="text" name="order[words_min]" id="words-min" value="%s" class="medium" /></label><br />
                    <label for="words-max" class="leftLabel">%s <input type="text" name="order[words_max]" id="words-max" value="%s" class="medium" /></label><br />
                </fieldset>
                <fieldset class="right half">
                    <dl class="tb-list">
                        <dt>%s:</dt><dd>%s</dd>
                        <dt>%s:</dt><dd id="cost_word_count">&nbsp;</dd>
                        <dt>%s:</dt><dd id="cost_per_word">&nbsp;</dd>
                        <dt>%s:</dt><dd id="cost_order">&nbsp;</dd>
                        <dt>%s:</dt><dd id="cost_tb">&nbsp;</dd>
                        <dt>%s:</dt><dd id="cost_total">&nbsp;</dd>
                    </dl>
                </fieldset>
                <fieldset style="clear:both">
                    <label for="order-title">%s</label>
                    <input type="text" name="order[order_title]" value="%s" id="order-title" class="long" />
                </fieldset>
                <fieldset style="clear:both">
                    <label for="order-text">%s</label>
                    <textarea name="order[order_text]" id="order-text" cols="60" rows="10" class="long">%s</textarea>
                </fieldset>
                <fieldset style="clear:both">
                    <p class="submit">
                        <input type="submit" value="%s" class="button right" />
                    </p>
                </fieldset>
            </form>
            <div style="clear:both;"></div>
        ';

        $args   = array(
            __('New order', parent::getIdentifier()),
            $update ? attribute_escape(add_query_arg('_wpnonce', wp_create_nonce(parent::ACTION_ORDER_UPDATE))) : attribute_escape(add_query_arg('_wpnonce', wp_create_nonce(parent::ACTION_ORDER_INSERT))),
            $budgetId,
            $budgetName,
            $budgetId,
            $update ? $_REQUEST[parent::PARAM_ORDER_ID] : null,
            __('Order category', parent::getIdentifier()),
            $this->showCategories($this->getCategories($budgetId), isset($aOrder['category_id']) ? $aOrder['category_id'] : null),
            __('Rating', parent::getIdentifier()),
            $this->showRating(isset($aOrder['rating']) ? $aOrder['rating'] : null),
            __('Due days', parent::getIdentifier()),
            $this->showDueDays(isset($aOrder['duedays']) ? $aOrder['duedays'] : null),
            __('Words minimum', parent::getIdentifier()),
            isset($aOrder['words_min']) ? $aOrder['words_min'] : null,
            __('Words maximum', parent::getIdentifier()),
            isset($aOrder['words_max']) ? $aOrder['words_max'] : null,
            __('Budget', parent::getIdentifier()),
            $budgetName,
            __('Word count', parent::getIdentifier()),
            __('Cost per word', parent::getIdentifier()),
            __('Max. cost words', parent::getIdentifier()),
            __('Cost textbroker', parent::getIdentifier()),
            __('Max. cost', parent::getIdentifier()),
            __('Order title', parent::getIdentifier()),
            isset($aOrder['order_title']) ? $aOrder['order_title'] : null,
            __('Order text', parent::getIdentifier()),
            isset($aOrder['order_text']) ? $aOrder['order_text'] : null,
            __('Create order', parent::getIdentifier()),
        );

        if ($display) {
            vprintf($str, $args);
        } else {
            return vsprintf($str, $args);
        }
    }

    /**
     *
     *
     * @param array $aOrders
     * @return string | void
     */
    private function showOrderInformation(array $aOrders, $display = false) {

        $str    = '
            <h4>%s</h4>
            <table class="widefat">
                <thead>
                    <th>%s (%s)</th>
                    <th>%s</th>
                    <th>%s (%s)</th>
                    <th>%s</th>
                    <th>%s</th>
                </thead>
                <tbody>
                    %s
                </tbody>
            </table>
        ';

        $args   = array(
            __('Orders', parent::getIdentifier()),
            __('Order id', parent::getIdentifier()),
            __('Budget', parent::getIdentifier()),
            __('Title', parent::getIdentifier()),
            __('Order date', parent::getIdentifier()),
            __('Handling time', parent::getIdentifier()),
            __('Status', parent::getIdentifier()),
            __('Actions', parent::getIdentifier()),
            $this->showOrders($aOrders),
        );

        if ($display) {
            vprintf($str, $args);
        } else {
            return vsprintf($str, $args);
        }
    }

    /**
     *
     *
     * @param array $aOrders
     * @return string
     */
    private function showOrders(array $aOrders) {

        $str = '';

        if (count($aOrders) > 0) {
            foreach ($aOrders as $order) {
                $str .= sprintf('
                    <tr class="">
                        <td>%s (%s)</td>
                        <td>%s</td>
                        <td>%s (%s %s)</td>
                        <td><a href="%s&%s&%s" title="%s - %s: %s">%s</a></td>
                        <td>%s</td>
                    </tr>
                ',
                    $order['budget_order_id'],
                    $order['budget_id'],
                    $order['order_title'],
                    $order['order_date'],
                    $order['duedays'],
                    $order['duedays'] > 1 ? __('days', parent::getIdentifier()) : __('day', parent::getIdentifier()),
                    attribute_escape(add_query_arg('_wpnonce', wp_create_nonce(parent::ACTION_ORDER_GETSTATUS))), parent::PARAM_BUDGET_ID . '=' . $order['budget_id'], parent::PARAM_ORDER_ID . '=' . $order['budget_order_id'],
                    __('Klick to refresh status', parent::getIdentifier()),
                    __('Current status', parent::getIdentifier()),
                    $order['status']['budget_order_status_id'],
                    $order['status']['budget_order_status'],
                    $this->showActions($order['status']['budget_order_status_id'],
                    $order['budget_order_id'])
                );
            }
        } else {
            $str = sprintf('<tr class="alt act"><td colspan="5">%s</td></tr>', __('No orders', parent::getIdentifier()));
        }

        return $str;
    }

    /**
     *
     *
     * @param array $aCategories
     * @param mixed $selected
     * @return string
     */
    private function showCategories(array $aCategories, $selected) {

        $str = '';

        foreach ($aCategories as $label => $id) {
            if ($id == $selected) {
                $str .= sprintf('<option value="%s" selected="selected">%s</option>', $id, $label);
            } else {
                $str .= sprintf('<option value="%s">%s</option>', $id, $label);
            }
        }

        return $str;
    }

    /**
     *
     *
     * @param mixed $selected
     * @return string
     */
    private function showRating($selected) {

        $str        = '';

        foreach (range(2, 5) as $value) {
            if ($value == $selected) {
                $str .= sprintf('<option value="%s" selected="selected">%s</option>', $value, __('Rating ' . $value, parent::getIdentifier()));
            } else {
                $str .= sprintf('<option value="%s">%s</option>', $value, __('Rating ' . $value, parent::getIdentifier()));
            }
        }

        return $str;
    }

    /**
     *
     *
     * @return string
     */
    private function showReview() {

        $str        = '';

        foreach (range(0, 4) as $value) {
            $str .= sprintf('<option value="%s">%s</option>', $value, __('Review ' . $value, parent::getIdentifier()));
        }

        return $str;
    }

    /**
     *
     *
     * @param mixed $selected
     * @return string
     */
    private function showDueDays($selected) {

        $str        = '';

        foreach (range(1, 10) as $value) {
            if ($value == $selected) {
                $str .= sprintf('<option value="%s" selected="selected">%s</option>', $value, $value);
            } else {
                $str .= sprintf('<option value="%s">%s</option>', $value, $value);
            }
        }

        return $str;
    }

    /**
     *
     *
     * @param int $status
     * @param int $orderId
     * @return string
     */
    private function showActions($status, $orderId) {

        $aActions = array(
            Textbroker::TB_STATUS_PLACED            => array(
                'delete',
            ),
            Textbroker::TB_STATUS_TB_ACCEPTED       => array(
                'delete',
            ),
            Textbroker::TB_STATUS_INWORK            => array(
            ),
            Textbroker::TB_STATUS_READY             => array(
                'preview',
                #'accept',
            ),
            Textbroker::TB_STATUS_ACCEPTED          => array(
                'publish',
            ),
            Textbroker::TB_STATUS_DELIVERED         => array(
                'publish',
            ),
            Textbroker::TB_STATUS_DELETED           => array(
                'remove'
            ),
            Textbroker::TB_STATUS_REJECTION_GRANTED => array(
                'delete'
            ),
            Textbroker::TB_STATUS_ORDER_REFUSED     => array(
                'edit',
                'delete'
            ),
            Textbroker::TB_STATUS_WAITING           => array(
            ),
        );

        $str = '';

        foreach ($aActions[$status] as $action) {
            switch ($action) {
                case 'accept' :
                    $str .= sprintf(
                        '<a href="%s&%s">%s</a>',
                        attribute_escape(add_query_arg('_wpnonce', wp_create_nonce(parent::ACTION_ORDER_ACCEPT))), parent::PARAM_ORDER_ID . '=' . $orderId,
                        __('Accept', parent::getIdentifier()));
                    break;
                case 'edit' :
                    $str .= sprintf(
                        '<a href="%s&%s">%s</a>',
                        attribute_escape(add_query_arg('_wpnonce', wp_create_nonce(parent::ACTION_ORDER_EDIT))), parent::PARAM_ORDER_ID . '=' . $orderId,
                        __('Re-create order', parent::getIdentifier()));
                    break;
                case 'delete' :
                    $str .= sprintf(
                        '<a href="%s&%s">%s</a>',
                        attribute_escape(add_query_arg('_wpnonce', wp_create_nonce(parent::ACTION_ORDER_DELETE))), parent::PARAM_ORDER_ID . '=' . $orderId,
                        __('Delete', parent::getIdentifier()));
                    break;
                case 'preview' :
                    $str .= sprintf(
                        '<a href="%s&%s">%s</a>',
                        attribute_escape(add_query_arg('_wpnonce', wp_create_nonce(parent::ACTION_ORDER_PREVIEW))), parent::PARAM_ORDER_ID . '=' . $orderId,
                        __('Preview', parent::getIdentifier()));
                    break;
                case 'publish' :
                    $aOrder = $this->getOrder($orderId);

                    if (!isset($aOrder['published_id']) || !is_numeric($aOrder['published_id'])) {
                        $str .= sprintf(
                            '<form action="%s" method="post">
                                <input type="hidden" name="%s" value="%s"/>
                                <select name="%s" class="medium">
                                    <option value="%s">%s</option>
                                    <option value="%s">%s</option>
                                </select>
                                <input type="submit" name="" value="%s" />
                            </form>',
                            attribute_escape(add_query_arg('_wpnonce', wp_create_nonce(parent::ACTION_ORDER_PUBLISH))),
                            parent::PARAM_ORDER_ID,
                            $orderId,
                            parent::PARAM_PUBLISH_TYPE,
                            parent::PUBLISH_TYPE_POST,
                            __('Post', parent::getIdentifier()),
                            parent::PUBLISH_TYPE_PAGE,
                            __('Page', parent::getIdentifier()),
                            __('Publish', parent::getIdentifier())
                        );
                    } else {
                        $str .= sprintf(
                            '<a href="%s&%s">%s</a>',
                            attribute_escape(add_query_arg('_wpnonce', wp_create_nonce(parent::ACTION_ORDER_REMOVE))), parent::PARAM_ORDER_ID . '=' . $orderId,
                            __('Remove', parent::getIdentifier())
                        );
                    }
                    break;
                case 'remove' :
                    $str .= sprintf(
                        '<a href="%s&%s">%s</a>',
                        attribute_escape(add_query_arg('_wpnonce', wp_create_nonce(parent::ACTION_ORDER_REMOVE))), parent::PARAM_ORDER_ID . '=' . $orderId,
                        __('Remove', parent::getIdentifier())
                    );
                    break;
            }
            $str .= ' ';
        }

        $str = trim($str);

        return $str;
    }

    /**
     *
     *
     * @param array $aOrder
     * @param bool $display
     * @return string
     */
    private function showPreview(array $aOrder, $display = false) {

        $str = '
            <h3>%s: „%s“</h3>
            <h4>%s</h4>
            <div>
                <p>
                    %s
                </p>
                <table class="widefat">
                    <tr>
                        <td>
                            %s: %s
                        </td>
                        <td>
                            %s: %s
                        </td>
                        <td>
                            %s: %s
                        </td>
                    </tr>
                </table>
                <form action="%s" method="post" class="tb">
                    <label for="order-comment">%s:</label>
                    <textarea id="order-comment" name="order_comment" class="long"></textarea>
                    <label for="order-review">%s:</label>
                    <select id="order-review" name="order_review">
                        %s
                    </select>
                    <input type="submit" value="%s">
                    %s
                </form>
            </div>
            <div>
                <h4>%s</h4>
                <form action="%s" method="post" class="tb">
                    <fieldset>
                        <label for="comment">%s</label>
                        <textarea name="%s" rows="5" id="comment" class="full"></textarea>
                        <p class="submit">
                            <input type="submit" class="button right" value="%s" />
                        </p>
                    </fieldset>
                </form>
            </div>
        ';

        $args = array(
            __('Preview', parent::getIdentifier()),
            $aOrder['your_title'],
            $aOrder['title'],
            $aOrder['text'],
            __('Classification', parent::getIdentifier()),
            $aOrder['classification'],
            __('Count words', parent::getIdentifier()),
            $aOrder['count_words'],
            __('Author', parent::getIdentifier()),
            $aOrder['author'],
            attribute_escape(add_query_arg('_wpnonce', wp_create_nonce(parent::ACTION_ORDER_ACCEPT))),
            __('Comment', parent::getIdentifier()),
            __('Review', parent::getIdentifier()),
            $this->showReview(),
            __('Accept', parent::getIdentifier()),
            $aOrder['is_revised'] ? $this->showRejectLink($aOrder['order_id']) : null,
            __('Request revision', parent::getIdentifier()),
            attribute_escape(add_query_arg('_wpnonce', wp_create_nonce(parent::ACTION_ORDER_REVISE))),
            __('Your comment', parent::getIdentifier()),
            parent::PARAM_COMMENT,
            __('Revise', parent::getIdentifier()),
        );

        if ($display) {
            vprintf($str, $args);
        } else {
            return vsprintf($str, $args);
        }
    }

    /**
     *
     *
     * @param int $orderId
     * @param bool $display
     * @return string
     */
    private function showRejectLink($orderId, $display = false) {

        $str = '
            <a href="%s&%s">%s</a>
        ';

        $args = array(
            attribute_escape(add_query_arg('_wpnonce', wp_create_nonce(parent::ACTION_ORDER_REJECT))),
            parent::PARAM_ORDER_ID . '=' . $orderId,
            __('Reject', parent::getIdentifier())
        );

        if ($display) {
            vprintf($str, $args);
        } else {
            return vsprintf($str, $args);
        }
    }
}

/**
 *
 * @package Textbroker WordPress-Plugin
 * @since PHP5.2.12
 */
class TextbrokerOrderPublishException extends TextbrokerOrderException {

    public function __construct($message, $code = 0) {

        parent::__construct($message, $code);
    }
}

/**
 *
 * @package Textbroker WordPress-Plugin
 * @since PHP5.2.12
 */
class TextbrokerOrderUpdateException extends TextbrokerOrderException {

    public function __construct($message, $code = 0) {

        parent::__construct($message, $code);
    }
}

/**
 *
 * @package Textbroker WordPress-Plugin
 * @since PHP5.2.12
 */
class TextbrokerOrderInsertException extends TextbrokerOrderException {

    public function __construct($message, $code = 0) {

        parent::__construct($message, $code);
    }
}

/**
 *
 * @package Textbroker WordPress-Plugin
 * @since PHP5.2.12
 */
class TextbrokerOrderReviseException extends TextbrokerOrderException {

    public function __construct($message, $code = 0) {

        parent::__construct($message, $code);
    }
}

/**
 *
 * @package Textbroker WordPress-Plugin
 * @since PHP5.2.12
 */
class TextbrokerOrderException extends Exception {

    public function __construct($message, $code = 0) {

        parent::__construct($message, $code);
    }
}
?>
