Mocha Child Theme
http://www.studiopress.com/themes/mocha

INSTALL: 
1. Upload the Mocha child theme folder via FTP to your wp-content/themes/ directory. (the Genesis parent theme needs to be in the wp-content/themes/ directory as well)
2. Go to your WordPress dashboard and select Appearance.
3. Activate the Mocha theme.
4. Inside your WordPress dashboard, go to Genesis > Theme Settings and configure them to your liking.

WIDGETS:
Primary Sidebar - This is the primary sidebar if you are using the Content/Sidebar, Sidebar/Content, Content/Sidebar/Sidebar, Sidebar/Sidebar/Content or Sidebar/Content/Sidebar Site Layout option.
Secondary Sidebar - This is the secondary sidebar if you are using the Content/Sidebar/Sidebar, Sidebar/Sidebar/Content or Sidebar/Content/Sidebar Site Layout option.

FEATURED IMAGES
By default WordPress will create a default thumbnail image for each image you upload and the size can be specified in your dashboard under Settings > Media. In addition, the Mocha child theme creates the following thumbnail images you'll see below, which are defined (and can be modified) in the functions.php file. These are the recommended thumbnail sizes that are used on the Mocha child theme demo site.

sidebar-alt - 125px by 125px

SUPPORT
If you are looking for theme support, please visit http://www.studiopress.com/support.