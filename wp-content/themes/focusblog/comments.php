<?php
if (in_category(5)){
?>
<div style="border-radius: 40px; margin-top: 20px !important; margin-left: -20px !important; margin-right: -20px !important;" class="tve_cb tve_cb1 tve_green">
<div class="tve_hd tve_cb_cnt">
<h3 class="">
Start Your Records Search:</h3>
<span></span>
</div>
<div class="tve_cb_cnt">
<div class="thrv_wrapper thrv_custom_html_shortcode"><!-- Special version of Bootstrap that only affects content wrapped in .bootstrap-iso -->
<link href="http://backgroundchecks.org/wp-content/uploads/2016/09/bootstrap-iso.css" rel="stylesheet">
<div class="bootstrap-iso">
<div class="container-fluid"><div class="container-fluid"><div class="search-image-wrapper" style="text-align: center;">
    <img src="http://backgroundchecks.org/wp-content/uploads/2016/09/searchheader.png" alt="">  
  </div>
  
    <form action="http://backgroundchecks.org/results/" method="post" class="form-inline">
      <div class="form-group col-md-3 col-sm-3">
        <label for="FName" class="control-label">First Name</label>
        <input type="text" placeholder="" name="fname" id="FName" class="form-control">
      </div>
      <div class="form-group col-md-3 col-sm-3">
        <label for="LName" class="control-label">Last Name</label>
        <input type="text" placeholder="" name="lname" id="LName" class="form-control">
      </div>
      <div class="form-group col-md-3 col-sm-3">
      <label for="state" class="control-label">
        Which State?
      </label>
      <select name="state" id="select" class="select form-control">
      <option value="Everywhere">Entire USA</option>
      <option value="AL">Alabama</option>
      <option value="AK">Alaska</option>
      <option value="AZ">Arizona</option>
      <option value="AR">Arkansas</option>
      <option value="CA">California</option>
      <option value="CO">Colorado</option>
      <option value="CT">Connecticut</option>
      <option value="DE">Delaware</option>
      <option value="DC">District Of Columbia</option>
      <option value="FL">Florida</option>
      <option value="GA">Georgia</option>
      <option value="HI">Hawaii</option>
      <option value="ID">Idaho</option>
      <option value="IL">Illinois</option>
      <option value="IN">Indiana</option>
      <option value="IA">Iowa</option>
      <option value="KS">Kansas</option>
      <option value="KY">Kentucky</option>
      <option value="LA">Louisiana</option>
      <option value="ME">Maine</option>
      <option value="MD">Maryland</option>
      <option value="MA">Massachusetts</option>
      <option value="MI">Michigan</option>
      <option value="MN">Minnesota</option>
      <option value="MS">Mississippi</option>
      <option value="MO">Missouri</option>
      <option value="MT">Montana</option>
      <option value="NE">Nebraska</option>
      <option value="NV">Nevada</option>
      <option value="NH">New Hampshire</option>
      <option value="NJ">New Jersey</option>
      <option value="NM">New Mexico</option>
      <option value="NY">New York</option>
      <option value="NC">North Carolina</option>
      <option value="ND">North Dakota</option>
      <option value="OH">Ohio</option>
      <option value="OK">Oklahoma</option>
      <option value="OR">Oregon</option>
      <option value="PA">Pennsylvania</option>
      <option value="RI">Rhode Island</option>
      <option value="SC">South Carolina</option>
      <option value="SD">South Dakota</option>
      <option value="TN">Tennessee</option>
      <option value="TX">Texas</option>
      <option value="UT">Utah</option>
      <option value="VT">Vermont</option>
      <option value="VA">Virginia</option>
      <option value="WA">Washington</option>
      <option value="WV">West Virginia</option>
      <option value="WI">Wisconsin</option>
      <option value="WY">Wyoming</option>
      </select>
    </div>
    <div class="form-group col-md-3 col-sm-3">
      <button class="btn-lg btn-success" id="search-submit-button" type="submit" style="margin-top: 18px;">Start Searching</button>  
</div></form>
</div>
</div>
</div>
</div>
</div>
<?} ?>

<?php

global $post;

$lazy_load_comments = thrive_get_theme_options("comments_lazy");

$enable_fb_comments = thrive_get_theme_options("enable_fb_comments");

$fb_app_id = thrive_get_theme_options("fb_app_id");

?>



<?php if ($lazy_load_comments == 1): ?>

    <script type="text/javascript">

        _thriveCurrentPost = <?php echo json_encode(get_the_ID()); ?>;

    </script>





<?php endif; ?>

<?php tha_comments_before(); ?>

	
<?php if ($enable_fb_comments != "only_fb"): ?>

    <article id="comments">

        <?php if (comments_open() && !post_password_required()) : ?>

            <div class="ctb">

                <span class="ftx txt_thrive_link_to_comments"><?php _e("Click Here to Leave a Comment Below", 'thrive'); ?></span>

                <span class="cmt lcm" href=""> <span class="str"></span> <?php echo get_comments_number(); ?> <?php _e("comments", 'thrive'); ?> </span>

                <div class="clear"></div>

            </div>

        <?php endif; ?>

        <div class="awr">

            <?php if ($lazy_load_comments != 1):

                thrive_theme_comment_nav();

            endif; ?>



            <div class="cmb" style="margin-left: 0px;" id="thrive_container_list_comments">

                <?php if ($lazy_load_comments != 1): ?>

                    <?php wp_list_comments(array('callback' => 'thrive_comments')); ?>

                <?php endif; ?>

            </div><!-- /comment_list -->



            <?php if ($lazy_load_comments != 1):

                thrive_theme_comment_nav();

            endif; ?>



            <?php if (comments_open() && !post_password_required()) : ?>

                <?php if ($lazy_load_comments == 1): ?>

                    <div class="ctb ctr" style="display: none;" id="thrive_container_preload_comments">

                        <img class="preloader" src="<?php echo get_template_directory_uri() ?>/images/loading.gif" alt=""/>

                    </div>

                <?php endif; ?>

                <div class="lrp" id="thrive_container_form_add_comment" <?php if ($lazy_load_comments == 1): ?>style="display:none;"<?php endif; ?>>

                    <h4><?php _e("Leave a Reply:", 'thrive'); ?></h4>

                    <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">

                        <?php if (!is_user_logged_in()): ?>

                            <input type="text" placeholder="<?php _e("Name*", "thrive")?>" id="author" author="author" class="text_field author" name="author" />

                            <input type="text" placeholder="<?php _e("Email*", "thrive") ?>" id="email" author="email" class="text_field email" name="email" />

                            <input type="text" placeholder="<?php _e("Website", "thrive")?>" id="website" author="website" class="text_field website lst" name="url" />

                        <?php endif; ?>

                        <textarea id="comment" name="comment" class="textarea"></textarea>

                        <input type="submit" value="<?php _e("SUBMIT", 'thrive'); ?>" />

                        <?php comment_id_fields(); ?>

                        <?php do_action('comment_form', $post->ID); ?>

                    </form>

                </div>        

            <?php elseif ((!comments_open() || post_password_required()) && get_comments_number() > 0): ?>

                <div class="no_comm">

                    <h4 class="ctr">

                        <?php _e("Comments are closed", 'thrive'); ?>

                    </h4>

                </div>            

            <?php endif; ?>

        </div>

    </article>

<?php endif; ?>



<?php if (($enable_fb_comments == "only_fb" || $enable_fb_comments == "both_fb_regular" || (!comments_open() && $enable_fb_comments == "fb_when_disabled")) && !empty($fb_app_id)) : ?>

    <article id="comments_fb" style="min-height: 100px; border: 1px solid #ccc;">

        <div class="fb-comments" data-href="<?php echo get_permalink(get_the_ID()); ?>" data-numposts="<?php echo thrive_get_theme_options("fb_no_comments") ?>" data-width="100%" data-colorscheme="<?php echo thrive_get_theme_options("fb_color_scheme") ?>"></div>

    </article>

<?php endif; ?>

<?php tha_comments_after(); ?>



