// Helper function to get cookie from current page
function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    } else {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
        end = dc.length;
        }
    }
    return decodeURI(dc.substring(begin + prefix.length, end));
} 

(function($){
    $(document).ready(function() {
        var cookie = getCookie("mcook");
        if (cookie == '1') {
            // remove 
            $('.tve_cb.tve_cb1.tve_green').remove();
            // remove 
            $('.thrv_wrapper.thrv_custom_html_shortcode').remove();
        } 
    });
})(jQuery); 
