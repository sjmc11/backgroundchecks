function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
}

(function($){
  $(document).ready(function() {
    // Set mcook=1 when the page is loaded
    setCookie('mcook', '1', 365)
  });
})(jQuery); 
