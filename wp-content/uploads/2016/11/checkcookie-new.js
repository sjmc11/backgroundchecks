// Helper function to get cookie from current page
function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    } else {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
        end = dc.length;
        }
    }
    return decodeURI(dc.substring(begin + prefix.length, end));
} 

(function($){
    $(document).ready(function() {
        var cookie = getCookie("mcook");
        if (cookie == '1') {
            // Hide the search box on the homepage
            $('.tve_cb.tve_cb1.tve_green').hide();
        } 
    });
})(jQuery); 
