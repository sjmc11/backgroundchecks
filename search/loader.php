<?php
$currentPage = 'loader'; // Set the current page to loader

include_once 'inc/functions.php'; // Calling the functions file
include_once 'inc/urls.php'; // Calling the URLs file

// Getting query string parameters and sanitizing them
$firstName = !empty($_GET['firstname']) ? sanitizeParameters($_GET['firstname']) : FALSE;
$lastName = !empty($_GET['lastname']) ? sanitizeParameters($_GET['lastname']) : FALSE;
$state = !empty($_GET['state']) ? sanitizeParameters(strtoupper($_GET['state']), 'state') : 'ALL';

// When mandatory fields are set assign first and last names to the fullName, otherwise redirect to the homepage
if(!empty($firstName) && !empty($lastName))
{
	$fullName = ' for <i>'. ucfirst($firstName) .' '. ucfirst($lastName) .'</i>';
	$stateText = $state == 'ALL' ? 'All States' : $state;
}else
{
	header('location: '. $baseURL); // Redirecting to the homepage
	exit;
}

$metaTitle = 'Searching for '. ucfirst($firstName) .' '. ucfirst($lastName) .' in '. $stateText .' - BackgroundChecks.org';
$teaserURL .= '?firstname='. $firstName .'&lastname='. $lastName .'&state='. $state;

include_once 'inc/header.php'; // Calling the header file
?>
<body>
<div id="container">
	<?php
	include_once 'inc/headerMenu.php'; // Calling the header menu file
	?>
	<!-- Loader 1 -->
	<div class="loader">
		<div class="wrapper cf">
			<div class="loader-container cf">
				<h1 class="loader-title l1-block">Searching All Records<?= $fullName; ?> in <i><?= $stateText; ?></i></h1>
				<div class="loader-progress-cont l1-block">
					<div class="loader-progress"><div style="width:0%;"></div></div>
					<div class="loader-mugshots"></div>
				</div>
				<div class="l1-container l1-block cf">
					<div class="l1-content">
						<div class="l1-content-inn">
							<h2 class="l1-title">We are searching for...</h2>
							<div class="l1-list cf">
								<ul id="loader-list">
									<li><i class="loader-circle"></i>Arrests &amp; Warrants</li>
									<li><i class="loader-circle"></i>Mugshots</li>
									<li><i class="loader-circle"></i>Criminal Records</li>
									<li><i class="loader-circle"></i>Convictions</li>
									<li><i class="loader-circle"></i>Jail Records</li>
									<li><i class="loader-circle"></i>Court Records</li>
									<li><i class="loader-circle"></i>Traffic Tickets</li>
									<li><i class="loader-circle"></i>Bankruptcies</li>
									<li><i class="loader-circle"></i>Legal Judgments</li>
									<li><i class="loader-circle"></i>Divorce Records</li>
									<li><i class="loader-circle"></i>Phone &amp; Address</li>
									<li><i class="loader-circle"></i>Hidden Social Profiles</li>
									<li><i class="loader-circle"></i>Email Address</li>
									<li><i class="loader-circle"></i>Marriage Records</li>
									<li><i class="loader-circle"></i>Assets</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="l1-sidebar">
						<h2 class="l1-title">We are checking...</h2>
						<div class="l1-list2 cf">
							<ul>
								<li class="icn-01">Federal Data Sources</li>
								<li class="icn-02">State Data Sources</li>
								<li class="icn-03">County Data Sources</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- // Loader 1 -->

<?php
include_once 'inc/footer.php'; // Calling the footer file
?>