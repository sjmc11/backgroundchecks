<?php
// Assigning header menu, footer and main URLs
$baseURL = 'http://backgroundchecks.org/';
$homeURL = 'http://search.backgroundchecks.org/';
$loaderURL = $homeURL .'loader.php';
$loader2URL = $homeURL .'loader2.php';
$teaserURL = $homeURL .'results.php';
$selectionURL = $homeURL .'selection.php';
$registerURL = $homeURL .'register.php';
$loginURL = 'http://members.backgroundchecks.org/customer/login';
$contactURL = 'http://members.backgroundchecks.org/customer/help';
$newsRoomURL = $baseURL .'blog';

$affEmail = 'ari@wiredrhino.com';
$apiKey = '96a27ace9d4bf5ece192979058b5b7659f4d188c';
$xmlURL = 'http://teasers.infopay.com/teaser/affPeopleXml?user='. $affEmail .'&key='. $apiKey;

$sourcesURL = 'http://teasers.infopay.com/teaser/affPoliceSourceXml?user='. $affEmail .'&key='. $apiKey;

$mercID = '57c418b3c601d5ad5a2ecb6f';
$checkoutURL = 'https://checkout.infopay.com/checkout/?merc_id='. $mercID;

// Prices array containing all information
$prices = array(
	'19.95' => array('items' => '57c496a09cf5c5db9878263c', 'sku' => 'backgrchecks/monthly1995', 'rebill' => TRUE, 'duration' => '1 Month', 'subsequentDuration'=> '1 Month', 'price' => '$19.95', 'subsequentPrice' => '$19.95'), // $19.95/month
	'4.95' => array('items' => '57c497489cf5c5db9878263d', 'sku' => 'backgrchecks/5day-495-1995', 'rebill' => TRUE, 'duration' => '5 days', 'subsequentDuration'=> '1 Month', 'price' => '$4.95', 'subsequentPrice' => '$19.95'), // $4.95 - $19.95/month
	'29.95' => array('items' => '57c4984d9cf5c5db9878263e', 'sku' => 'backgrchecks/2995', 'price' => '$29.95'), // $29.95
	'free' => array('items' => '57c58ef69cf5c57c5092b9cb', 'sku' => 'backgrchecks/5dayfree-1995')
);

// Looping through the price array and assigning the disclaimer text
while(current($prices) !== FALSE)
{
	if($prices[key($prices)]['rebill'])
	{
		$disclaimer[key($prices)] = 'You are receiving a '. $prices[key($prices)]['duration'] .' unlimited trial access for '. $prices[key($prices)]['price'] .'. If you decide to cancel within '. $prices[key($prices)]['duration'] .' you will only be charged this amount. Afterwards you will be charged '. $prices[key($prices)]['subsequentPrice'] .' every '. $prices[key($prices)]['subsequentDuration'] .'. Cancel anytime.';
	}

	next($prices);
}
?>