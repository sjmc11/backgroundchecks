$(document).ready(function(){
	// Remove the error class from the inputs if the user fills in some info
	$( "input[type=text]" ).on('keyup keydown keypress change paste', function() {
		if( $( this ).val() != '' ){
			$( this ).removeClass( 'error' ).next('span').remove();
		}
	});
	function validateForm($this){

		if( $("input[name='firstname']", $this).val() == '' ){
			$("input[name='firstname']", $this).addClass('error').focus().after('<span class="form-error">Please enter a first name</span>');
			return false;
		}

		if( $("input[name='lastname']", $this).val() == '' ){
			$("input[name='lastname']", $this).addClass('error').focus().after('<span class="form-error">Please enter a last name</span>');
			return false;
		}
	}

	// on form submit call the validateForm function
	 $('form').submit(function(){
		return validateForm($(this));
	});
});