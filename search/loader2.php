<?php
$currentPage = 'loader2'; // Set the current page to loader

include_once 'inc/functions.php'; // Calling the functions file
include_once 'inc/urls.php'; // Calling the URLs file

// Getting query string parameters and sanitizing them
$firstName = !empty($_GET['firstname']) ? sanitizeParameters($_GET['firstname']) : FALSE;
$lastName = !empty($_GET['lastname']) ? sanitizeParameters($_GET['lastname']) : FALSE;
$city = !empty($_GET['city']) ? sanitizeParameters($_GET['city']) : FALSE;
$state = !empty($_GET['state']) ? sanitizeParameters(strtoupper($_GET['state']), 'state') : 'ALL';
$address = !empty($_GET['address']) ? sanitizeParameters($_GET['address']) : FALSE;
$age = !empty($_GET['age']) ? sanitizeParameters($_GET['age']) : FALSE;
$total = !empty($_GET['total']) ? sanitizeParameters($_GET['total']) : FALSE;
$datasources = !empty($_GET['datasources']) ? sanitizeParameters($_GET['datasources']) : FALSE;

// When mandatory fields are set assign first and last names to the fullName, otherwise redirect to the homepage
if(!empty($firstName) && !empty($lastName))
{
	$fullName = ' for <i>'. ucfirst($firstName) .' '. ucfirst($lastName) .'</i>';
	$onText = ' on '. ucfirst($firstName) .' '. ucfirst($lastName);
	$stateText = $state == 'ALL' ? 'All States' : $state;
}else
{
	header('location: '. $baseURL); // Redirecting to the homepage
	exit;
}

$metaTitle = 'Searching for '. ucfirst($firstName) .' '. ucfirst($lastName) .' in '. $stateText .' - BackgroundChecks.org';
$selectionURL .= '?firstname='. $firstName .'&lastname='. $lastName .'&state='. $state;

// When city available append it to the selection URL
if(!empty($city))
{
	$selectionURL .= '&city='. $city;
}

// When address available append it to the selection URL
if(!empty($address))
{
	$selectionURL .= '&address='. $address;
}

// When age available append it to the selection URL
if(!empty($age))
{
	$selectionURL .= '&age='. $age;
}

// When datasources available append it to the selection URL
if(!empty($datasources))
{
	$selectionURL .= '&datasources='. $datasources;
}

// When total available append it to the selection URL
if(!empty($total))
{
	$selectionURL .= '&total='. $total;
}

include_once 'inc/header.php'; // Calling the header file
?>
<body>
<div id="container">
	<?php
	include_once 'inc/headerMenu.php'; // Calling the header menu file
	?>
	<!-- Loader 2 -->
	<div class="loader">
		<div class="wrapper cf">
			<div class="loader-container cf">
				<h1 class="loader-title l1-block">We are preparing your report!</h1>
				<h2 class="loader-subtitle l1-block">Accessing records from all Massachusetts data sources<?= $fullName; ?></h2>
				<div class="l1-list3 l1-block cf">
					<div class="cell">
						<i id="circle1" class="loader-circle2"></i>
						<div class="text">Connecting to Federal, State and County Databases</div>
					</div>
					<div class="cell">
						<i id="circle2" class="loader-circle2"></i>
						<div class="text">Extracting Data</div>
					</div>
					<div class="cell">
						<i id="circle3" class="loader-circle2"></i>
						<div class="text">Building Your Report</div>
					</div>
				</div>
				<div class="loader-progress-cont l1-block">
					<div class="loader-progress"><div style="width:0%;"></div></div>
				</div>
				<div class="l1-note"><span>Information<?= $onText; ?> is Available...</span></div>
			</div>
		</div>
	</div>
	<!-- // Loader 2 -->

<?php
include_once 'inc/footer.php'; // Calling the footer file
?>